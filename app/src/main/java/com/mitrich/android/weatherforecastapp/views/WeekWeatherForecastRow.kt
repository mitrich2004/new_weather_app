package com.mitrich.android.weatherforecastapp.views

import android.annotation.SuppressLint
import com.mitrich.android.weatherforecastapp.R
import com.mitrich.android.weatherforecastapp.data.DayWeatherForecast
import com.squareup.picasso.Picasso
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.week_weather_forecast_row.view.*

class WeekWeatherForecastRow(private val dayWeatherForecast: DayWeatherForecast) : Item<ViewHolder>() {
    override fun getLayout() = R.layout.week_weather_forecast_row

    @SuppressLint("SetTextI18n")
    override fun bind(viewHolder: ViewHolder, position: Int) {
        Picasso.get().load(dayWeatherForecast.weatherIconUrl).into(viewHolder.itemView.week_weather_forecast_image)
        viewHolder.itemView.week_weather_forecast_day_text.text = dayWeatherForecast.dayName
        viewHolder.itemView.week_weather_forecast_description_text.text = dayWeatherForecast.weatherDescription
        viewHolder.itemView.week_weather_forecast_temp_text.text = "${dayWeatherForecast.maxTemp}°/${dayWeatherForecast.minTemp}°"
    }
}
