package com.mitrich.android.weatherforecastapp.data

data class LocationCurrentWeather (
    val weatherIcon: String?,
    val weatherDescription: String,
    val locationName: String,
    val currentTemp: String,
    val lat: String,
    val lng: String
)