package com.mitrich.android.weatherforecastapp.data

data class UserData(
    var measureSystem: String,
    var backgroundColorId: Int
)