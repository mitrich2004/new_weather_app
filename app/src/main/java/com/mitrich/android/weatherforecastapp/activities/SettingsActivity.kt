package com.mitrich.android.weatherforecastapp.activities

import android.content.Intent
import android.os.Bundle
import android.widget.RadioButton
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.mitrich.android.weatherforecastapp.R
import com.mitrich.android.weatherforecastapp.activities.MainActivity.Companion.backgroundColorId
import com.mitrich.android.weatherforecastapp.activities.MainActivity.Companion.measureSystem
import com.mitrich.android.weatherforecastapp.activities.MainActivity.Companion.measureSystemSpeedSign
import com.mitrich.android.weatherforecastapp.activities.MainActivity.Companion.measureSystemTempSign
import com.mitrich.android.weatherforecastapp.data.UserData
import kotlinx.android.synthetic.main.activity_settings.*
import maes.tech.intentanim.CustomIntent
import org.json.JSONObject
import java.io.File

class SettingsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        File("$filesDir/${MainActivity.userdataFileName}").forEachLine {
            val userData = Gson().fromJson(it, UserData::class.java)
            measureSystem = userData.measureSystem
            backgroundColorId = userData.backgroundColorId
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        when(backgroundColorId) {
            R.color.blueBackground -> blue_button.isChecked = true
            R.color.greenBackground -> green_button.isChecked = true
            R.color.orangeBackground -> orange_button.isChecked = true
            R.color.redBackground -> red_button.isChecked = true
            R.color.purpleBackground -> purple_button.isChecked = true
        }

        settings_activity_layout.setBackgroundResource(backgroundColorId)

        val radioButtons = arrayListOf<RadioButton>(blue_button, green_button, orange_button, red_button, purple_button)

        back_image.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
            CustomIntent.customType(this, "right-to-left")
        }

        if (measureSystem == "metric") metric_system_checkbox.isChecked = true
        else imperial_system_checkbox.isChecked = true


        metric_system_checkbox.setOnClickListener {
            metric_system_checkbox.isChecked = true
            imperial_system_checkbox.isChecked = false
            measureSystem = "metric"
            measureSystemTempSign = "°C"
            measureSystemSpeedSign = "m/s"
            File("$filesDir/${MainActivity.userdataFileName}").writeText(
                JSONObject(Gson().toJson(UserData(measureSystem, backgroundColorId))).toString()
            )
        }

        imperial_system_checkbox.setOnClickListener {
            imperial_system_checkbox.isChecked = true
            metric_system_checkbox.isChecked = false
            measureSystem = "imperial"
            measureSystemTempSign = "°F"
            measureSystemSpeedSign = "mph"
            File("$filesDir/${MainActivity.userdataFileName}").writeText(
                JSONObject(Gson().toJson(UserData(measureSystem, backgroundColorId))).toString()
            )
        }

        blue_button.setOnClickListener {
            backgroundColorId = R.color.blueBackground
            settings_activity_layout.setBackgroundResource(backgroundColorId)
            for (radioButton in radioButtons) {
                if (radioButton.id != R.id.blue_button) radioButton.isChecked = false
            }
            File("$filesDir/${MainActivity.userdataFileName}").writeText(
                JSONObject(Gson().toJson(UserData(measureSystem, backgroundColorId))).toString()
            )
        }

        green_button.setOnClickListener {
            backgroundColorId = R.color.greenBackground
            settings_activity_layout.setBackgroundResource(backgroundColorId)
            for (radioButton in radioButtons) {
                if (radioButton.id != R.id.green_button) radioButton.isChecked = false
            }
            File("$filesDir/${MainActivity.userdataFileName}").writeText(
                JSONObject(Gson().toJson(UserData(measureSystem, backgroundColorId))).toString()
            )
        }

        orange_button.setOnClickListener {
            backgroundColorId = R.color.orangeBackground
            settings_activity_layout.setBackgroundResource(backgroundColorId)
            for (radioButton in radioButtons) {
                if (radioButton.id != R.id.orange_button) radioButton.isChecked = false
            }
            File("$filesDir/${MainActivity.userdataFileName}").writeText(
                JSONObject(Gson().toJson(UserData(measureSystem, backgroundColorId))).toString()
            )
        }

        red_button.setOnClickListener {
            backgroundColorId = R.color.redBackground
            settings_activity_layout.setBackgroundResource(backgroundColorId)
            for (radioButton in radioButtons) {
                if (radioButton.id != R.id.red_button) radioButton.isChecked = false
            }
            File("$filesDir/${MainActivity.userdataFileName}").writeText(
                JSONObject(Gson().toJson(UserData(measureSystem, backgroundColorId))).toString()
            )
        }

        purple_button.setOnClickListener {
            backgroundColorId = R.color.purpleBackground
            settings_activity_layout.setBackgroundResource(backgroundColorId)
            for (radioButton in radioButtons) {
                if (radioButton.id != R.id.purple_button) radioButton.isChecked = false
            }
            File("$filesDir/${MainActivity.userdataFileName}").writeText(
                JSONObject(Gson().toJson(UserData(measureSystem, backgroundColorId))).toString()
            )
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this, MainActivity::class.java))
        CustomIntent.customType(this, "right-to-left")
    }
}
