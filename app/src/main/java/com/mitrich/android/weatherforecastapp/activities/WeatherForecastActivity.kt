package com.mitrich.android.weatherforecastapp.activities

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.mitrich.android.weatherforecastapp.R
import com.mitrich.android.weatherforecastapp.activities.MainActivity.Companion.backgroundColorId
import com.mitrich.android.weatherforecastapp.activities.MainActivity.Companion.measureSystem
import com.mitrich.android.weatherforecastapp.activities.MainActivity.Companion.weatherLocationLat
import com.mitrich.android.weatherforecastapp.activities.MainActivity.Companion.weatherLocationLng
import com.mitrich.android.weatherforecastapp.utils.WeatherDataUtils
import com.mitrich.android.weatherforecastapp.views.WeekWeatherForecastRow
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_weather_forecast.*
import kotlinx.android.synthetic.main.activity_weather_forecast.weather_forecast_recycler_view
import maes.tech.intentanim.CustomIntent
import okhttp3.*
import org.json.JSONObject
import java.io.IOException

class WeatherForecastActivity : AppCompatActivity() {

    private val weatherDataUtils = WeatherDataUtils()

    override fun onCreate(savedInstanceState: Bundle?) {
        loadWeekForecastData()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_weather_forecast)

        weather_forecast_activity_layout.setBackgroundResource(backgroundColorId)

        back_image.setOnClickListener {
            finish()
            CustomIntent.customType(this, "fadein-to-fadeout")
        }
    }

    private fun loadWeekForecastData() {
        val dailyWeatherForecastRequest = Request
            .Builder()
            .url("https://api.openweathermap.org/data/2.5/onecall?lat=$weatherLocationLat&lon=$weatherLocationLng&exclude=current,minutely,hourly&units=$measureSystem&appid=51bc4edf9aec5e561a58a174eb2df68e")
            .build()

        val dailyWeatherForecastClient = OkHttpClient()
        dailyWeatherForecastClient.newCall(dailyWeatherForecastRequest).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                TODO()
            }

            @SuppressLint("SetTextI18n")
            override fun onResponse(call: Call, response: Response) {
                val body = response.body!!.string()
                response.close()
                val weatherForecastJSONObject = JSONObject(body)
                val sevenDaysWeatherForecast = weatherDataUtils.getDaysWeatherForecast(weatherForecastJSONObject, 7)
                runOnUiThread {
                    val weatherForecastAdapter = GroupAdapter<ViewHolder>()
                    weather_forecast_recycler_view.adapter = weatherForecastAdapter
                    if (sevenDaysWeatherForecast != null) {
                        sevenDaysWeatherForecast.forEach { weatherForecastAdapter.add(WeekWeatherForecastRow(it)) }
                        no_forecast_available_text.visibility = View.GONE
                    } else no_forecast_available_text.visibility = View.VISIBLE

                    loading_panel.visibility = View.GONE
                }
            }
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
        CustomIntent.customType(this, "fadein-to-fadeout")
    }
}
