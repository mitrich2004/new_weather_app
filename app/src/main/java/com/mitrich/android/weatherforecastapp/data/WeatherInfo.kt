package com.mitrich.android.weatherforecastapp.data

data class WeatherInfo(
    val iconId: Int,
    val title: String,
    val value: String
)