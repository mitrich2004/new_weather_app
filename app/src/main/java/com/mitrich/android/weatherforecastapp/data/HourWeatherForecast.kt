package com.mitrich.android.weatherforecastapp.data

data class HourWeatherForecast(
    val hour: String,
    val iconUrl: String,
    val temp: String
)