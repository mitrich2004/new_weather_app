package com.mitrich.android.weatherforecastapp.activities

import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.Window
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.location.LocationServices
import com.google.gson.Gson
import com.mitrich.android.weatherforecastapp.R
import com.mitrich.android.weatherforecastapp.activities.MainActivity.Companion.backgroundColorId
import com.mitrich.android.weatherforecastapp.activities.MainActivity.Companion.currentLocationIsShown
import com.mitrich.android.weatherforecastapp.activities.MainActivity.Companion.locationStatus
import com.mitrich.android.weatherforecastapp.activities.MainActivity.Companion.locationsList
import com.mitrich.android.weatherforecastapp.activities.MainActivity.Companion.locationsListFileName
import com.mitrich.android.weatherforecastapp.activities.MainActivity.Companion.measureSystem
import com.mitrich.android.weatherforecastapp.activities.MainActivity.Companion.weatherCurrentLat
import com.mitrich.android.weatherforecastapp.activities.MainActivity.Companion.weatherCurrentLng
import com.mitrich.android.weatherforecastapp.activities.MainActivity.Companion.weatherLocationName
import com.mitrich.android.weatherforecastapp.data.LocationCurrentWeather
import com.mitrich.android.weatherforecastapp.data.WeatherLocation
import com.mitrich.android.weatherforecastapp.utils.WeatherDataUtils
import com.mitrich.android.weatherforecastapp.views.LocationWeatherView
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_locations.*
import maes.tech.intentanim.CustomIntent
import okhttp3.*
import org.json.JSONObject
import java.io.File
import java.io.IOException
import java.util.*

class LocationsActivity : AppCompatActivity() {

    private val mainActivity = MainActivity()
    private val weatherDataUtils = WeatherDataUtils()
    private var selected = false

    val locationsListAdapter = GroupAdapter<ViewHolder>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_locations)

        locations_activity_layout.setBackgroundResource(backgroundColorId)

        locations_recycler_view.adapter = locationsListAdapter

        locationsListAdapter.setOnItemClickListener { item, _ ->
            if (!selected) {
                val location = item as LocationWeatherView
                currentLocationIsShown =
                    locationStatus && locationsListAdapter.getAdapterPosition(item) == 0
                if (!currentLocationIsShown) {
                    weatherLocationName = location.locationCurrentWeather.locationName
                }
                mainActivity.updateLatLng(
                    location.locationCurrentWeather.lat,
                    location.locationCurrentWeather.lng,
                    false
                )
                val intent = Intent(this, MainActivity::class.java)
                intent.putExtra("firstRun", false)
                startActivity(intent)
                CustomIntent.customType(this, "left-to-right")
            }
        }

        locationsListAdapter.setOnItemLongClickListener { item, view ->
            if (locationStatus && locationsListAdapter.getAdapterPosition(item) == 0) {
                showToast("Current location can't be deleted", this)
            } else {
                if (!selected) {
                    if (locationsListAdapter.itemCount > 1) {
                        val locationWeatherView = item as LocationWeatherView
                        val locationName = locationWeatherView.locationCurrentWeather.locationName
                        val locationViewIndex = getLocationIndex(locationName)

                        selected = true
                        deleteLocationViewGraphics(view)

                        delete_layout.setOnClickListener {
                            locationsList.remove(locationViewIndex)
                            File("$filesDir/$locationsListFileName").writeText(locationsList.toString())
                            locationsListAdapter.remove(item)
                            selected = false
                            deleteLocationViewGraphics(view)
                        }

                        cancel_image.setOnClickListener {
                            selected = false
                            deleteLocationViewGraphics(view)
                        }
                    } else {
                        showToast("The only location can't be deleted", this)
                    }
                }
            }

            true
        }

        if (locationStatus) {
            location_image.setImageResource(R.drawable.ic_location_on)
            addCurrentLocationView()
        } else {
            location_image.setImageResource(R.drawable.ic_location_off)
            if (locationsList.length() == 0) addLocationView(
                "37.386051",
                "-122.083855",
                "California"
            )
        }

        for (i in 0 until locationsList.length()) {
            val weatherLocation = Gson().fromJson(
                locationsList.getJSONObject(i).toString(),
                WeatherLocation::class.java
            )
            addLocationView(
                weatherLocation.locationLat,
                weatherLocation.locationLng,
                weatherLocation.locationName
            )
            Thread.sleep(150)
        }

        location_image.setOnClickListener {
            if (locationStatus) showToast("Location is enable", this)
            else {
                if (ContextCompat.checkSelfPermission(
                        this,
                        ACCESS_FINE_LOCATION
                    ) != PERMISSION_GRANTED
                )
                    ActivityCompat.requestPermissions(this, arrayOf(ACCESS_FINE_LOCATION), 1)
                else showToast("Fail to access location!", this)
            }
        }

        add_location_button.setOnClickListener {
            if (locationsList.length() < 5) {
                val addLocationDialog = Dialog(this)
                addLocationDialog.apply {
                    requestWindowFeature(Window.FEATURE_NO_TITLE)
                    setCancelable(true)
                    setContentView(R.layout.add_location_dialog)
                    window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                    val editText = findViewById<EditText>(R.id.editText)
                    val saveText = findViewById<TextView>(R.id.okay_text)
                    editText.addTextChangedListener(object : TextWatcher {
                        override fun afterTextChanged(s: Editable?) {
                        }

                        override fun beforeTextChanged(
                            s: CharSequence?,
                            start: Int,
                            count: Int,
                            after: Int
                        ) {
                        }

                        override fun onTextChanged(
                            s: CharSequence?,
                            start: Int,
                            before: Int,
                            count: Int
                        ) {
                            if (editText.text.toString()
                                    .isBlank()
                            ) saveText.setTextColor(ContextCompat.getColor(this@LocationsActivity, R.color.disabledSaveTextColor))
                            else saveText.setTextColor(ContextCompat.getColor(this@LocationsActivity, R.color.activeSaveTextColor))
                        }

                    })

                    saveText.setOnClickListener {
                        if (!editText.text.toString().isBlank()) {
                            val locationName = editText.text.toString().trim()
                            val geocoder = Geocoder(this@LocationsActivity, Locale.ENGLISH)
                            val addresses = geocoder.getFromLocationName(locationName, 1)
                            if (addresses.isNotEmpty()) {
                                val address = addresses[0]
                                val latitude = address.latitude.toString()
                                val longitude = address.longitude.toString()
                                if (!checkLocationInList(latitude, longitude)) {
                                    addLocationView(latitude, longitude, locationName)
                                    locationsList.put(
                                        JSONObject(
                                            Gson().toJson(
                                                WeatherLocation(
                                                    locationName,
                                                    latitude,
                                                    longitude
                                                )
                                            ).toString()
                                        )
                                    )
                                    File("$filesDir/$locationsListFileName").writeText(locationsList.toString())
                                    dismiss()
                                } else {
                                    showToast(
                                        "This location's been already added",
                                        this@LocationsActivity
                                    )
                                    editText.setText("")
                                }
                            } else {
                                showToast("Fail to define location", this@LocationsActivity)
                                editText.setText("")
                            }
                        }
                    }
                    show()
                }
            } else {
                showToast("You can't have for than 5 locations", this)
            }
        }
    }

    private fun addCurrentLocationView() {
        val currentWeatherRequest = Request
            .Builder()
            .url("https://api.openweathermap.org/data/2.5/weather?lat=$weatherCurrentLat&lon=$weatherCurrentLng&units=$measureSystem&appid=51bc4edf9aec5e561a58a174eb2df68e")
            .build()

        val currentWeatherClient = OkHttpClient()
        currentWeatherClient.newCall(currentWeatherRequest).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                TODO()
            }

            @SuppressLint("SetTextI18n")
            override fun onResponse(call: Call, response: Response) {
                val body = response.body!!.string()
                response.close()
                val currentWeatherJSONObject = JSONObject(body)
                val weatherIcon = weatherDataUtils.getWeatherIcon(currentWeatherJSONObject)
                val currentTemp = weatherDataUtils.getCurrentTemp(currentWeatherJSONObject)
                val weatherDescription =
                    weatherDataUtils.getWeatherDescription(currentWeatherJSONObject)
                runOnUiThread {
                    locationsListAdapter.add(
                        0,
                        LocationWeatherView(
                            LocationCurrentWeather(
                                weatherIcon,
                                weatherDescription,
                                "Current",
                                currentTemp,
                                weatherCurrentLat,
                                weatherCurrentLng
                            )
                        )
                    )
                    loading_circle.visibility = View.GONE
                }
            }
        })
    }

    private fun addLocationView(lat: String, lng: String, locationName: String) {
        val currentWeatherRequest = Request
            .Builder()
            .url("https://api.openweathermap.org/data/2.5/weather?lat=$lat&lon=$lng&units=$measureSystem&appid=51bc4edf9aec5e561a58a174eb2df68e")
            .build()

        val currentWeatherClient = OkHttpClient()
        currentWeatherClient.newCall(currentWeatherRequest).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                TODO()
            }

            @SuppressLint("SetTextI18n")
            override fun onResponse(call: Call, response: Response) {
                val body = response.body!!.string()
                response.close()
                val currentWeatherJSONObject = JSONObject(body)
                val weatherIcon = weatherDataUtils.getWeatherIcon(currentWeatherJSONObject)
                val currentTemp = weatherDataUtils.getCurrentTemp(currentWeatherJSONObject)
                val weatherDescription =
                    weatherDataUtils.getWeatherDescription(currentWeatherJSONObject)
                runOnUiThread {
                    locationsListAdapter.add(
                        LocationWeatherView(
                            LocationCurrentWeather(
                                weatherIcon,
                                weatherDescription,
                                locationName,
                                currentTemp,
                                lat,
                                lng
                            )
                        )
                    )
                    loading_circle.visibility = View.GONE
                }
            }
        })
    }

    private fun getLocationIndex(locationName: String): Int {
        var index = 1
        for (i in 0 until locationsList.length()) {
            val location = locationsList.getJSONObject(i)
            val name = location.getString("locationName")
            if (name == locationName) {
                index = i
            }
        }
        return index
    }

    private fun deleteLocationViewGraphics(view: View) {
        if (selected) {
            view.setBackgroundResource(R.drawable.selected_layout_background)
            locations_list_toolbar.setBackgroundResource(R.color.selectedWidgetBackgroundColor)
            cancel_image.visibility = View.VISIBLE
            add_location_button.visibility = View.GONE
            delete_layout.visibility = View.VISIBLE
            location_image.visibility = View.GONE
        } else {
            view.setBackgroundResource(R.drawable.round_corners_layout_background)
            locations_list_toolbar.setBackgroundResource(R.color.widgetBackgroundColor)
            cancel_image.visibility = View.GONE
            add_location_button.visibility = View.VISIBLE
            delete_layout.visibility = View.GONE
            location_image.visibility = View.VISIBLE
        }
    }

    private fun checkLocationInList(lat: String, lng: String): Boolean {
        for (i in 0 until locationsList.length()) {
            val weatherLocation = Gson().fromJson(
                locationsList.getJSONObject(i).toString(),
                WeatherLocation::class.java
            )
            val locationInList =
                weatherLocation.locationLat == lat && weatherLocation.locationLng == lng
            if (locationInList) {
                return true
            }
        }
        return false
    }

    private fun showToast(text: String, context: Context) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show()
    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1 && grantResults.isNotEmpty()) {
            if (grantResults[0] == PERMISSION_GRANTED) {
                val fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
                fusedLocationClient.lastLocation
                    .addOnSuccessListener { location: Location? ->
                        locationStatus = if (location != null) {
                            weatherCurrentLat = location.latitude.toString()
                            weatherCurrentLng = location.longitude.toString()
                            mainActivity.updateLatLng(weatherCurrentLat, weatherCurrentLng, false)
                            location_image.setImageResource(R.drawable.ic_location_on)
                            addCurrentLocationView()
                            true
                        } else {
                            showToast("Fail to access location!", this)
                            false
                        }
                    }
            } else {
                showToast("Permission denied!", this)
                locationStatus = false
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
        CustomIntent.customType(this, "left-to-right")
    }
}
