package com.mitrich.android.weatherforecastapp.data

data class WeatherLocation(
    val locationName: String,
    val locationLat: String,
    val locationLng: String
)