package com.mitrich.android.weatherforecastapp.utils

import android.annotation.SuppressLint
import java.sql.Date
import java.text.SimpleDateFormat
import java.util.*

class DateDataUtils {
    @SuppressLint("SimpleDateFormat")
    fun getHourFromUnixTimestamp(timestamp: Int): String {
        val date = Date(timestamp * 1000L)
        val sdf = SimpleDateFormat("H:mm")
        return sdf.format(date)
    }

    @SuppressLint("SimpleDateFormat")
    fun getDateFromUnixTimestamp(timestamp: Int): Date {
        return Date(timestamp * 1000L)
    }

    @SuppressLint("SimpleDateFormat")
    fun getDayNameFromDate(date: Date): String {
        val sdf = SimpleDateFormat("EEEE", Locale.ENGLISH)
        return sdf.format(date)
    }
}
