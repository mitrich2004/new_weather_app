package com.mitrich.android.weatherforecastapp.activities

import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.location.LocationServices
import com.google.gson.Gson
import com.mitrich.android.weatherforecastapp.R
import com.mitrich.android.weatherforecastapp.data.UserData
import com.mitrich.android.weatherforecastapp.data.WeatherLocation
import com.mitrich.android.weatherforecastapp.utils.WeatherDataUtils
import com.mitrich.android.weatherforecastapp.views.DailyWeatherForecastRow
import com.mitrich.android.weatherforecastapp.views.HourlyWeatherForecastColumn
import com.mitrich.android.weatherforecastapp.views.WeatherInfoRow
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_main.*
import maes.tech.intentanim.CustomIntent
import okhttp3.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.File
import java.io.IOException
import kotlin.system.exitProcess

class MainActivity : AppCompatActivity() {

    companion object {
        var weatherLocationLat = "37.386051"
        var weatherLocationLng = "-122.083855"
        var weatherCurrentLat = "1.0"
        var weatherCurrentLng = "1.0"

        var locationStatus = false
        var currentLocationIsShown = true

        var locationsList = JSONArray()

        var backgroundColorId = R.color.blueBackground

        var weatherLocationName = "Unknown"
        var measureSystem = "metric"
        var measureSystemTempSign = "°C"
        var measureSystemSpeedSign = "m/s"

        const val userdataFileName = "userData.json"
        const val locationsListFileName = "locationsList.json"
    }

    private val weatherDataUtils = WeatherDataUtils()

    val moreWeatherInfoAdapter = GroupAdapter<ViewHolder>()
    val hourlyWeatherForecastAdapter = GroupAdapter<ViewHolder>()
    val dailyWeatherForecastAdapter = GroupAdapter<ViewHolder>()

    override fun onCreate(savedInstanceState: Bundle?) {

        if (!File("$filesDir/$locationsListFileName").exists()) {
            File.createTempFile("locationsList", ".json")
            File("$filesDir/$locationsListFileName").writeText(JSONArray().toString())
        } else {
            File("$filesDir/$locationsListFileName").forEachLine {
                locationsList = JSONArray(it)
            }
        }

        if (!File("$filesDir/$userdataFileName").exists()) {
            File.createTempFile("userData", ".json")
            File("$filesDir/$userdataFileName").writeText(JSONObject(Gson().toJson(UserData("metric", R.color.blueBackground))).toString())
        } else {
            File("$filesDir/$userdataFileName").forEachLine {
                val userData = Gson().fromJson(it, UserData::class.java)
                measureSystem = userData.measureSystem
                backgroundColorId = userData.backgroundColorId
                if (measureSystem == "metric") {
                    measureSystemTempSign = "°C"
                    measureSystemSpeedSign = "m/s"
                } else {
                    measureSystemTempSign = "°F"
                    measureSystemSpeedSign = "mph"
                }
            }
        }

        val firstRun = intent.getBooleanExtra("firstRun", true)

        if (firstRun) {
            if (ContextCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, arrayOf(ACCESS_FINE_LOCATION), 1)
            } else {
                fetchCurrentLocation()
            }
        } else loadAllData()


        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        current_weather_background_layout.setBackgroundResource(backgroundColorId)

        more_information_recycler_view.adapter = moreWeatherInfoAdapter
        hourly_forecast_recycler_view.adapter = hourlyWeatherForecastAdapter
        weather_forecast_recycler_view.adapter = dailyWeatherForecastAdapter

        swipe_refresh_layout.setOnRefreshListener {
            loadAllData()
        }

        cities_list_image.setOnClickListener {
            startActivity(Intent(this, LocationsActivity::class.java))
            CustomIntent.customType(this, "right-to-left")
        }

        settings_image.setOnClickListener {
            startActivity(Intent(this, SettingsActivity::class.java))
            CustomIntent.customType(this, "left-to-right")
        }

        weather_forecast_button.setOnClickListener {
            startActivity(Intent(this, WeatherForecastActivity::class.java))
            CustomIntent.customType(this, "fadein-to-fadeout")
        }

        if (firstRun && currentLocationIsShown) current_weather_city_name_text.text = ""
        else current_weather_city_name_text.text = weatherLocationName
    }

    private fun loadCurrentWeatherData() {
        val currentWeatherRequest = Request
            .Builder()
            .url("https://api.openweathermap.org/data/2.5/weather?lat=$weatherLocationLat&lon=$weatherLocationLng&units=$measureSystem&appid=51bc4edf9aec5e561a58a174eb2df68e")
            .build()

        val currentWeatherClient = OkHttpClient()
        currentWeatherClient.newCall(currentWeatherRequest).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                TODO()
            }

            @SuppressLint("SetTextI18n")
            override fun onResponse(call: Call, response: Response) {
                val body = response.body!!.string()
                response.close()
                val currentWeatherJSONObject = JSONObject(body)
                val weatherIconUrl = weatherDataUtils.getWeatherIcon(currentWeatherJSONObject)
                val currentTemperature = weatherDataUtils.getCurrentTemp(currentWeatherJSONObject)
                val currentWeatherDescription = weatherDataUtils.getWeatherDescription(currentWeatherJSONObject)

                runOnUiThread {
                    if (weatherIconUrl != null) Picasso.get().load(weatherIconUrl).into(current_weather_image_view)
                    else current_weather_image_view.setImageResource(R.drawable.not_found)

                    current_weather_temperature_text.text = currentTemperature
                    current_weather_description_text.text = currentWeatherDescription
                    progress_circular.visibility = View.GONE
                }
            }
        })
    }

    private fun loadDailyWeatherForecastData() {
        val dailyWeatherForecastRequest = Request
            .Builder()
            .url("https://api.openweathermap.org/data/2.5/onecall?lat=$weatherLocationLat&lon=$weatherLocationLng&exclude=current,minutely,hourly&units=$measureSystem&appid=51bc4edf9aec5e561a58a174eb2df68e")
            .build()

        val dailyWeatherForecastClient = OkHttpClient()
        dailyWeatherForecastClient.newCall(dailyWeatherForecastRequest).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                TODO()
            }

            @SuppressLint("SetTextI18n")
            override fun onResponse(call: Call, response: Response) {
                val body = response.body!!.string()
                response.close()
                val weatherForecastJSONObject = JSONObject(body)
                val threeDaysWeatherForecast =
                    weatherDataUtils.getDaysWeatherForecast(weatherForecastJSONObject, 3)
                runOnUiThread {
                    if (threeDaysWeatherForecast != null) {
                        threeDaysWeatherForecast.forEach { dailyWeatherForecastAdapter.add(DailyWeatherForecastRow(it)) }
                        no_data_available_text.visibility = View.GONE
                    } else no_data_available_text.visibility = View.VISIBLE
                }
            }
        })
    }

    private fun loadHourlyWeatherForecastData() {
        val hourlyWeatherForecastRequest = Request
            .Builder()
            .url("https://api.openweathermap.org/data/2.5/onecall?lat=$weatherLocationLat&lon=$weatherLocationLng&exclude=current,minutely,daily&units=$measureSystem&appid=51bc4edf9aec5e561a58a174eb2df68e")
            .build()

        val hourlyWeatherForecastClient = OkHttpClient()
        hourlyWeatherForecastClient.newCall(hourlyWeatherForecastRequest)
            .enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    TODO()
                }

                @SuppressLint("SetTextI18n")
                override fun onResponse(call: Call, response: Response) {
                    val body = response.body!!.string()
                    response.close()
                    val hourlyWeatherForecastJSONObject = JSONObject(body)
                    val dayHourlyWeatherForecastList = weatherDataUtils.getDayHourlyWeatherForecast(hourlyWeatherForecastJSONObject)
                    runOnUiThread {
                        if (dayHourlyWeatherForecastList != null) {
                            dayHourlyWeatherForecastList.forEach { hourlyWeatherForecastAdapter.add(HourlyWeatherForecastColumn(it)) }
                            no_data_text.visibility = View.GONE
                        } else no_data_text.visibility = View.VISIBLE
                    }
                }
            })
    }

    private fun loadMoreCurrentWeatherInfo() {
        val moreCurrentWeatherInfoRequest = Request
            .Builder()
            .url("https://api.openweathermap.org/data/2.5/weather?lat=$weatherLocationLat&lon=$weatherLocationLng&units=$measureSystem&appid=51bc4edf9aec5e561a58a174eb2df68e")
            .build()

        val moreCurrentWeatherInfoClient = OkHttpClient()
        moreCurrentWeatherInfoClient.newCall(moreCurrentWeatherInfoRequest)
            .enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    TODO()
                }

                @SuppressLint("SetTextI18n")
                override fun onResponse(call: Call, response: Response) {
                    val body = response.body!!.string()
                    response.close()
                    val moreCurrentWeatherInfoJSONObject = JSONObject(body)
                    val weatherInfoList = weatherDataUtils.getMoreWeatherInfo(moreCurrentWeatherInfoJSONObject)
                    runOnUiThread {
                        if (weatherInfoList != null) {
                            weatherInfoList.forEach { moreWeatherInfoAdapter.add(WeatherInfoRow(it)) }
                            no_data_text.visibility = View.GONE
                        } else no_info_text.visibility = View.VISIBLE
                        swipe_refresh_layout.isRefreshing = false
                    }
                }
            })
    }

    private fun loadTheCityNameByLatLng() {
        val geoCoderRequest = Request
            .Builder()
            .url("https://eu1.locationiq.com/v1/reverse.php?key=8f82dbf3ca219b&lat=$weatherLocationLat&lon=$weatherLocationLng&format=json")
            .build()

        val geoCoderClient = OkHttpClient()
        geoCoderClient.newCall(geoCoderRequest).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                TODO()
            }

            @SuppressLint("SetTextI18n")
            override fun onResponse(call: Call, response: Response) {
                val body = response.body!!.string()
                response.close()
                val geoCoderJSONObject = JSONObject(body)
                val cityName: String
                cityName = try {
                    geoCoderJSONObject.getJSONObject("address").getString("state")
                } catch (e: JSONException) {
                    try {
                        geoCoderJSONObject.getJSONObject("address").getString("city")
                    } catch (e: JSONException) {
                        "Unknown"
                    }
                }
                runOnUiThread {
                    current_weather_city_name_text.text = cityName
                }
            }
        })
    }

    fun updateLatLng(latitude: String, longitude: String, reloadData: Boolean) {
        weatherLocationLat = latitude
        weatherLocationLng = longitude
        if (reloadData) loadAllData()
    }

    private fun loadAllData() {
        loadCurrentWeatherData()
        loadDailyWeatherForecastData()
        loadHourlyWeatherForecastData()
        loadMoreCurrentWeatherInfo()
        if (currentLocationIsShown) {
            loadTheCityNameByLatLng()
        }
    }

    @SuppressLint("MissingPermission")
    private fun fetchCurrentLocation() {
        val fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                locationStatus = if (location != null) {
                    weatherCurrentLat = location.latitude.toString()
                    weatherCurrentLng = location.longitude.toString()
                    currentLocationIsShown = true
                    updateLatLng(weatherCurrentLat, weatherCurrentLng, true)
                    true
                } else {
                    Toast.makeText(this, "Fail to access location!", Toast.LENGTH_SHORT).show()
                    loadCurrentWeatherFromLocationsList()
                    false
                }
            }
    }

    private fun loadCurrentWeatherFromLocationsList() {
        if (locationsList.length() > 0) {
            val weatherLocation = Gson().fromJson(locationsList.getJSONObject(0).toString(), WeatherLocation::class.java)
            updateLatLng(weatherLocation.locationLat, weatherLocation.locationLng, true)
            weatherLocationName = weatherLocation.locationName
            currentLocationIsShown = false
        } else {
            weatherLocationName = "California"
            loadAllData()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1 && grantResults.isNotEmpty()) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                fetchCurrentLocation()
            } else {
                Toast.makeText(this, "Permission denied!", Toast.LENGTH_LONG).show()
                loadCurrentWeatherFromLocationsList()
                locationStatus = false
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
        exitProcess(1)
    }
}
