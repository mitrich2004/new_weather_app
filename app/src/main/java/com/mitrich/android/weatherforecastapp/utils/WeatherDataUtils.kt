package com.mitrich.android.weatherforecastapp.utils

import android.annotation.SuppressLint
import com.mitrich.android.weatherforecastapp.R
import com.mitrich.android.weatherforecastapp.activities.MainActivity.Companion.measureSystemSpeedSign
import com.mitrich.android.weatherforecastapp.activities.MainActivity.Companion.measureSystemTempSign
import com.mitrich.android.weatherforecastapp.data.DayWeatherForecast
import com.mitrich.android.weatherforecastapp.data.HourWeatherForecast
import com.mitrich.android.weatherforecastapp.data.WeatherInfo
import org.json.JSONException
import org.json.JSONObject
import kotlin.collections.ArrayList

class WeatherDataUtils {

    private val dateDataUtils = DateDataUtils()

    fun getWeatherIcon(weatherJSONObject: JSONObject): String? {
        return try {
            val weatherIconId= weatherJSONObject.getJSONArray("weather").getJSONObject(0).getString("icon")
            "http://openweathermap.org/img/wn/$weatherIconId@2x.png"
        } catch (e: JSONException) {
            null
        }
    }

    fun getCurrentTemp(currentWeatherJSONObject: JSONObject): String {
        return try {
            val currentTemperature = currentWeatherJSONObject.getJSONObject("main").getInt("temp")
            "$currentTemperature$measureSystemTempSign"
        } catch (error: JSONException) {
            "?"
        }
    }

    @SuppressLint("DefaultLocale")
    fun getWeatherDescription(weatherJSONObject: JSONObject): String {
        return try {
            val currentWeatherDescription = weatherJSONObject.getJSONArray("weather").getJSONObject(0).getString("main")
            currentWeatherDescription.capitalize()
        } catch (error: JSONException) {
            "Unknown"
        }
    }

    fun getDaysWeatherForecast(weatherForecastJSONObject: JSONObject, numberOfDays: Int): ArrayList<DayWeatherForecast>? {
        try {
            val dailyForecastJSONArray = weatherForecastJSONObject.getJSONArray("daily")
            val threeDaysWeatherForecastList: ArrayList<DayWeatherForecast> = ArrayList()
            for (dayNumber in 0 until numberOfDays) {
                val dayForecastJSONObject = dailyForecastJSONArray.getJSONObject(dayNumber)
                val date = dateDataUtils.getDateFromUnixTimestamp(dayForecastJSONObject.getInt("dt"))
                val maxTemp = dayForecastJSONObject.getJSONObject("temp").getInt("max").toString()
                val minTemp = dayForecastJSONObject.getJSONObject("temp").getInt("min").toString()
                val weatherIconId = dayForecastJSONObject.getJSONArray("weather").getJSONObject(0).getString("icon")
                val weatherIconUrl = "http://openweathermap.org/img/wn/$weatherIconId@2x.png"
                val weatherDescription = dayForecastJSONObject.getJSONArray("weather").getJSONObject(0).getString("main")
                var dayName = "Unknown"
                when (dayNumber) {
                    0 -> dayName = "Today"
                    1 -> dayName = "Tomorrow"
                    in (2..7) -> dayName = dateDataUtils.getDayNameFromDate(date)
                }
                val dayWeatherForecast =
                    DayWeatherForecast(
                        weatherIconUrl,
                        dayName,
                        date.toString(),
                        weatherDescription,
                        maxTemp,
                        minTemp
                    )
                threeDaysWeatherForecastList.add(dayNumber, dayWeatherForecast)
            }
            return threeDaysWeatherForecastList
        } catch (error: JSONException) {
            return null
        }
    }

    fun getDayHourlyWeatherForecast(weatherForecastJSONObject: JSONObject): ArrayList<HourWeatherForecast>? {
        try {
            val hourlyForecastJSONArray = weatherForecastJSONObject.getJSONArray("hourly")
            val twelveHoursWeatherForecastList: ArrayList<HourWeatherForecast> = ArrayList()
            for (hourNumber in 0 until 24) {
                val hourForecastJSONObject = hourlyForecastJSONArray.getJSONObject(hourNumber)
                val hour =
                    dateDataUtils.getHourFromUnixTimestamp(hourForecastJSONObject.getInt("dt"))
                val temp = hourForecastJSONObject.getInt("temp")
                val weatherIconId = hourForecastJSONObject.getJSONArray("weather").getJSONObject(0)
                    .getString("icon")
                val weatherIconUrl = "http://openweathermap.org/img/wn/$weatherIconId@2x.png"
                val hourWeatherForecast = HourWeatherForecast(hour, weatherIconUrl, temp.toString())
                twelveHoursWeatherForecastList.add(hourNumber, hourWeatherForecast)
            }
            return twelveHoursWeatherForecastList
        } catch (error: JSONException) {
            return null
        }
    }

    fun getMoreWeatherInfo(moreCurrentWeatherInfoJSONObject: JSONObject): ArrayList<WeatherInfo>? {
        val moreWeatherInfoList = arrayListOf<WeatherInfo>()
        try {
            val feelLikeTemp = moreCurrentWeatherInfoJSONObject.getJSONObject("main").getInt("feels_like")
            val humidity = moreCurrentWeatherInfoJSONObject.getJSONObject("main").getInt("humidity")
            val pressure = moreCurrentWeatherInfoJSONObject.getJSONObject("main").getInt("pressure")
            val windSpeed = moreCurrentWeatherInfoJSONObject.getJSONObject("wind").getInt("speed")

            moreWeatherInfoList.apply {
                add(WeatherInfo(R.drawable.thermometer, "Feels like","$feelLikeTemp$measureSystemTempSign"))
                add(WeatherInfo(R.drawable.humidity, "Humidity","$humidity%"))
                add(WeatherInfo(R.drawable.wind, "Wind speed","$windSpeed$measureSystemSpeedSign"))
                add(WeatherInfo(R.drawable.meter, "Pressure","${pressure}hPa"))
            }
        } catch (e: JSONException) {
            return null
        }
        return moreWeatherInfoList
    }
}
