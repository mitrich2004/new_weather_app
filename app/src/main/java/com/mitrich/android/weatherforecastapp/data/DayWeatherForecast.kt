package com.mitrich.android.weatherforecastapp.data

data class DayWeatherForecast(
    val weatherIconUrl: String,
    val dayName: String,
    val dayMonthDate: String,
    val weatherDescription: String,
    val maxTemp: String,
    val minTemp: String
)