package com.mitrich.android.weatherforecastapp.views

import com.mitrich.android.weatherforecastapp.R
import com.mitrich.android.weatherforecastapp.data.WeatherInfo
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.more_weather_info_row.view.*

class WeatherInfoRow(private val weatherInfo: WeatherInfo) : Item<ViewHolder>() {
    override fun getLayout() = R.layout.more_weather_info_row

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.weather_info_image_view.setImageResource(weatherInfo.iconId)
        viewHolder.itemView.weather_info_title.text = weatherInfo.title
        viewHolder.itemView.weather_info_value.text = weatherInfo.value
    }
}
