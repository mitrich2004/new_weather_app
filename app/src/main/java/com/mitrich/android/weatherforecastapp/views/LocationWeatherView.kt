package com.mitrich.android.weatherforecastapp.views

import android.annotation.SuppressLint
import android.view.View
import com.google.android.gms.internal.zzagy.runOnUiThread
import com.mitrich.android.weatherforecastapp.R
import com.mitrich.android.weatherforecastapp.activities.MainActivity.Companion.weatherCurrentLat
import com.mitrich.android.weatherforecastapp.activities.MainActivity.Companion.weatherCurrentLng
import com.mitrich.android.weatherforecastapp.data.LocationCurrentWeather
import com.squareup.picasso.Picasso
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.location_weather_view.view.*
import okhttp3.*
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException

class LocationWeatherView(val locationCurrentWeather: LocationCurrentWeather) : Item<ViewHolder>() {
    override fun getLayout() = R.layout.location_weather_view

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.location_temp_text.text = locationCurrentWeather.currentTemp
        viewHolder.itemView.location_weather_text.text = locationCurrentWeather.weatherDescription

        if (locationCurrentWeather.weatherIcon != null) Picasso.get().load(locationCurrentWeather.weatherIcon).into(viewHolder.itemView.location_weather_image)
        else viewHolder.itemView.location_weather_image.setImageResource(R.drawable.not_found)

        if (locationCurrentWeather.locationName != "Current") {
            viewHolder.itemView.location_name_text.text = locationCurrentWeather.locationName
            viewHolder.itemView.current_location_icon.visibility = View.GONE
        } else {
            val geoCoderRequest = Request
                .Builder()
                .url("https://eu1.locationiq.com/v1/reverse.php?key=8f82dbf3ca219b&lat=$weatherCurrentLat&lon=$weatherCurrentLng&format=json")
                .build()

            val geoCoderClient = OkHttpClient()
            geoCoderClient.newCall(geoCoderRequest).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    TODO()
                }

                @SuppressLint("SetTextI18n")
                override fun onResponse(call: Call, response: Response) {
                    val body = response.body!!.string()
                    response.close()
                    val geoCoderJSONObject = JSONObject(body)
                    val cityName: String
                    cityName = try {
                        geoCoderJSONObject.getJSONObject("address").getString("state")
                    } catch (e: JSONException) {
                        try {
                            geoCoderJSONObject.getJSONObject("address").getString("city")
                        } catch (e: JSONException) {
                            "Unknown"
                        }
                    }
                    runOnUiThread {
                        viewHolder.itemView.location_name_text.text = cityName
                        viewHolder.itemView.current_location_icon.visibility = View.VISIBLE
                    }
                }
            })
        }
    }
}
