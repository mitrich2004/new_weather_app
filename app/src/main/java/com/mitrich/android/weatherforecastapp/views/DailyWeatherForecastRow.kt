package com.mitrich.android.weatherforecastapp.views

import android.annotation.SuppressLint
import com.mitrich.android.weatherforecastapp.R
import com.mitrich.android.weatherforecastapp.data.DayWeatherForecast
import com.squareup.picasso.Picasso
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.weather_forecast_row.view.*

class DailyWeatherForecastRow(private val dayWeatherForecast: DayWeatherForecast) : Item<ViewHolder>() {
    override fun getLayout() = R.layout.weather_forecast_row

    @SuppressLint("SetTextI18n")
    override fun bind(viewHolder: ViewHolder, position: Int) {
        Picasso.get().load(dayWeatherForecast.weatherIconUrl).into(viewHolder.itemView.weather_image_view)
        viewHolder.itemView.day_name_weather_description_text.text = "${dayWeatherForecast.dayName} • ${dayWeatherForecast.weatherDescription}"
        viewHolder.itemView.max_min_temp_text.text = "${dayWeatherForecast.maxTemp}°/${dayWeatherForecast.minTemp}°"
    }
}
