package com.mitrich.android.weatherforecastapp.views

import android.annotation.SuppressLint
import com.mitrich.android.weatherforecastapp.R
import com.mitrich.android.weatherforecastapp.activities.MainActivity.Companion.measureSystemTempSign
import com.mitrich.android.weatherforecastapp.data.HourWeatherForecast
import com.squareup.picasso.Picasso
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.hourly_weather_forecast_column.view.*

class HourlyWeatherForecastColumn(private val hourWeatherForecast: HourWeatherForecast) : Item<ViewHolder>() {
    override fun getLayout() = R.layout.hourly_weather_forecast_column

    @SuppressLint("SetTextI18n")
    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.time_text.text = hourWeatherForecast.hour
        viewHolder.itemView.temp_text.text = "${hourWeatherForecast.temp}$measureSystemTempSign"
        Picasso.get().load(hourWeatherForecast.iconUrl).into(viewHolder.itemView.weather_image)
    }
}
